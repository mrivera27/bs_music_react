import React, { useContext, useState } from "react";
import { useHistory, Link } from "react-router-dom";
import { injectIntl, useIntl } from "react-intl";

import { authContext } from "../../context/contexts";
import Form from "../../components/Form";
import Logo from "../../components/Logo";
import Alert from "../../components/Alert";

const INIT_STATE = {
  email: "",
  password: "",
};

const Signin = () => {
  const { signinUser } = useContext(authContext);
  const { messages } = useIntl();
  const history = useHistory();
  const [values, setValues] = useState(INIT_STATE);
  const [alert, setAlert] = useState();

  const handleChange = (event) => {
    event.persist();
    setValues((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };

  const inputs = [
    {
      type: "text",
      placeholder: messages["user.signin.placeholder.email"],
      name: "email",
      value: values.email,
      onChange: handleChange,
    },
    {
      type: "password",
      placeholder: messages["user.signin.placeholder.password"],
      name: "password",
      value: values.password,
      onChange: handleChange,
    },
  ];

  const handleSubmit = async (event) => {
    event.preventDefault();
    const response = await signinUser(values);
    response.code ? setAlert(response.message) : history.go(0);
  };

  return (
    <>
      {alert && <Alert message={alert} setMessage={setAlert} />}
      <div className="user-layout__container--top">
        <Logo revert={true} />
      </div>
      <div className="user-layout__container--bottom container">
        <h1 className="signin__welcome--title">
          {messages["user.signin.submit.text"]}
        </h1>
        <p className="signin__welcome--text">
          {messages["user.signin.welcome.text"]}
        </p>
        <div className="signin__form">
          <Form
            inputs={inputs}
            buttonText={messages["user.signin.submit.text"]}
            onSubmit={handleSubmit}
          />
          <div className="signin__footer--redirect">
            <hr className="signin__footer--divider" />
            <p>{messages["user.signup.yet.text"]}</p>
            <Link to="/auth/signup" className="user-layout__container--link">
              {messages["user.signup.submit.text"]}
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default injectIntl(Signin);
