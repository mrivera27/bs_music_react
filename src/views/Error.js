import React from "react";
import { injectIntl, useIntl } from "react-intl";

import { APP_ROOT } from "../constants/defaultValues";

const Error = () => {
  const { messages } = useIntl();

  return (
    <main className="error-page">
      <div className="container error-page__container">
        <h1 className="error-page__title">{messages["error.page.title"]}</h1>
        <p className="error-page__message">{messages["error.page.message"]}</p>
        <a href={APP_ROOT} className="error-page__link">
          {messages["error.page.back-home"]}
        </a>
      </div>
    </main>
  );
};

export default injectIntl(Error);
