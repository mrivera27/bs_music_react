import {
  CURRENT_SONG,
  TOGGLE_RANDOM,
  TOGGLE_REPEAT,
  TOGGLE_PLAYING,
  SET_SONGS,
  SET_QUEUE,
} from "../actions";

export const setSongs = (songs) => ({
  type: SET_SONGS,
  payload: songs,
});

export const setQueue = (queue) => ({
  type: SET_QUEUE,
  payload: queue,
});

export const togglePlaying = (playing) => ({
  type: TOGGLE_PLAYING,
  payload: playing,
});

export const setCurrent = (id) => ({ type: CURRENT_SONG, payload: id });

export const toggleRepeat = (repeat) => ({
  type: TOGGLE_REPEAT,
  payload: !repeat,
});

export const toggleRandom = (random) => ({
  type: TOGGLE_RANDOM,
  payload: !random,
});

export const prevSong = (id, max) => setCurrent(id === 0 ? max - 1 : id - 1);

export const nextSong = (id, max) => setCurrent(id === max - 1 ? 0 : id + 1);

export const handleSongEnd = (random, repeat, id, max) => {
  if (random) {
    return {
      type: CURRENT_SONG,
      payload: ~~(Math.random() * max),
    };
  } else if (repeat || id < max - 1) {
    return nextSong(id, max);
  } else {
    return { type: "SONG_END", payload: false };
  }
};
