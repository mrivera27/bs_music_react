import React, { useReducer } from "react";
import Context from "./context";
import reducer from "./reducer";
import {
  setQueue,
  setSongs,
  nextSong,
  prevSong,
  setCurrent,
  toggleRandom,
  toggleRepeat,
  togglePlaying,
  handleSongEnd,
} from "./actions";

const INIT_STATE = {
  current: 0,
  songs: [],
  queue: [],
  repeat: false,
  random: false,
  playing: false,
  audio: null,
};

const PlayerState = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, INIT_STATE);

  return (
    <Context.Provider
      value={{
        currentSong: state.current,
        songs: state.songs,
        queue: state.queue,
        repeat: state.repeat,
        random: state.random,
        playing: state.playing,
        audio: state.audio,
        setSongs: (songs) => dispatch(setSongs(songs)),
        setQueue: (queue) => dispatch(setQueue(queue)),
        nextSong: () => dispatch(nextSong(state.current, state.songs.length)),
        prevSong: () => dispatch(prevSong(state.current, state.songs.length)),
        setCurrent: (id) => dispatch(setCurrent(id)),
        toggleRandom: () => dispatch(toggleRandom(state.random)),
        toggleRepeat: () => dispatch(toggleRepeat(state.repeat)),
        togglePlaying: (playing) => dispatch(togglePlaying(playing)),
        handleSongEnd: () =>
          dispatch(
            handleSongEnd(
              state.random,
              state.repeat,
              state.current,
              state.songs.length
            )
          ),
      }}
    >
      {children}
    </Context.Provider>
  );
};

export default PlayerState;
