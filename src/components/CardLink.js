import React from "react";
import { Link } from "react-router-dom";
import { injectIntl, useIntl } from "react-intl";
import { PlayArrow } from "@material-ui/icons";

const CardLink = ({ to, image, title, direction, className, children }) => {
  const { messages } = useIntl();

  return (
    <Link to={to} className={className}>
      <div className="card-link">
        <div className="card-link__overlay">
          <PlayArrow className="card-link__overlay--play" />
          {messages["app.home.song.play"]}
        </div>
        {image ? (
          <img
            src={image}
            alt={title}
            className={`card-link__image card-link__image--${direction}`}
          />
        ) : (
          <div>{children}</div>
        )}
        <h3 className="card-link__title">{title}</h3>
      </div>
    </Link>
  );
};

export default injectIntl(CardLink);
