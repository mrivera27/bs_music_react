import { CHANGE_THEME } from "../actions";
import { getCurrentColor } from "../../helpers/Utils";

const LOCALE_INIT_STATE = {
  locale: getCurrentColor(),
};

export default (state = LOCALE_INIT_STATE, action) => {
  switch (action.type) {
    case CHANGE_THEME:
      return { ...state, theme: action.payload };
    default:
      return { ...state };
  }
};
