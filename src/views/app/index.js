import React from "react";
import { Route, withRouter, Switch, Redirect } from "react-router-dom";

import AppLayout from "../../layout/AppLayout";
import Loader from "../../components/Loader";

const Home = React.lazy(() => import("./Home"));
const Results = React.lazy(() => import("./Results"));
const Channel = React.lazy(() => import("./Channel"));
const Settings = React.lazy(() => import("./Settings"));
const Favorites = React.lazy(() => import("./Favorites"));
const Playlist = React.lazy(() => import("./Playlist"));

const App = () => {
  return (
    <AppLayout>
      <Loader>
        <Switch>
          <Route exact path="/settings">
            <Settings />
          </Route>
          <Route exact path="/favorites">
            <Favorites />
          </Route>
          <Route exact path="/playlist">
            <Playlist />
          </Route>
          <Route exact path="/results">
            <Results />
          </Route>
          <Route exact path="/channel/:channelId">
            <Channel />
          </Route>
          <Route exact path="/">
            <Home />
          </Route>
          <Redirect to="/error" />
        </Switch>
      </Loader>
    </AppLayout>
  );
};

export default withRouter(App);
