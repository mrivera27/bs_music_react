import React from "react";
import { Link } from "react-router-dom";
import { PlayCircleOutline } from "@material-ui/icons";
import PropTypes from "prop-types";

const Logo = ({ revert }) => {
  return (
    <Link to="/" className={`logo logo--${revert ? "color-revert" : "none"}`}>
      <PlayCircleOutline className="logo__icon" />
      <h1 className="logo__text">Music</h1>
    </Link>
  );
};

Logo.propTypes = {
  revert: PropTypes.bool,
};

export default Logo;
