import React from "react";
import AudioState from "./audio";
import ThemeState from "./theme";
import LocaleState from "./locale";
import AuthState from "./auth";

const AppProvider = ({ children }) => {
  return (
    <AuthState>
      <LocaleState>
        <AudioState>
          <ThemeState>{children}</ThemeState>
        </AudioState>
      </LocaleState>
    </AuthState>
  );
};

export default AppProvider;
