import React, { Fragment, useState } from "react";
import { Link } from "react-router-dom";

const Dropdown = ({ options, children }) => {
  const [show, setShow] = useState(false);

  return (
    <div className="dropdown" onClick={() => setShow(!show)}>
      {children}
      <div className={`dropdown__content ${show ? "d-flex" : "d-none"}`}>
        {options &&
          options.map(({ text, to, icon, onClick }, index) => (
            <Fragment key={`opt-${index}`}>
              {onClick ? (
                <button
                  className=" dropdown__option--button dropdown__option--icon"
                  onClick={onClick}
                >
                  <span>{icon}</span>
                  <span>{text}</span>
                </button>
              ) : (
                <Link to={to} className="dropdown__option--icon">
                  <span>{icon}</span>
                  <span>{text}</span>
                </Link>
              )}
              {index + 1 !== options.length && (
                <div className="dropdown__option--divider" />
              )}
            </Fragment>
          ))}
      </div>
    </div>
  );
};

export default Dropdown;
