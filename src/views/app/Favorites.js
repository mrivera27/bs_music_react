import React, { useEffect, useState, useContext } from "react";
import { injectIntl, useIntl } from "react-intl";
import { useLocation, useHistory } from "react-router-dom";
import { PlayArrow, Shuffle } from "@material-ui/icons";

import TableList from "../../components/TableList";
import Firebase from "../../helpers/Firebase";
import { APP_ROOT } from "../../constants/defaultValues";
import { audioContext } from "../../context/contexts";
import { getRandomInt } from "../../helpers/Utils";

const queryString = require("querystring");

const Favorites = () => {
  const { messages } = useIntl();
  const location = useLocation();
  const history = useHistory();
  const [favorites, setFavorites] = useState([]);
  const { setSongs, setCurrent, togglePlaying } = useContext(audioContext);

  useEffect(() => {
    let isMounted = true;
    const query = queryString.parse(location.search.substr(1)).list;
    query
      ? Firebase.getRealtime(`favorites/${query}`, (snapshot) => {
          let data = [];
          snapshot.forEach((snap) => {
            data.push(snap.val());
          });
          if (isMounted) setFavorites(data);
        })
      : history.push(APP_ROOT);
    return () => {
      isMounted = false;
    };
  }, [location.search, history]);

  const handleShuffleClick = () => {
    const auxSongs = [...favorites];
    let shuffleList = [];
    while (shuffleList.length < favorites.length) {
      const index = getRandomInt(auxSongs.length);
      shuffleList.push(auxSongs.splice(index, 1)[0]);
    }
    setCurrent(0);
    togglePlaying(true);
    setSongs(shuffleList);
  };

  const handlePlayClick = (songId) => {
    const song = songId ? favorites.findIndex(({ id }) => id === songId) : 0;
    setCurrent(song);
    togglePlaying(true);
    setSongs(favorites);
  };

  return (
    <>
      {favorites && (
        <div className="channel">
          <div className="container-fluid__overlay">
            <div className="container">
              <h2 className="container__title channel__title">
                {messages["app.home.favorites.title"]}
              </h2>
              {favorites.length > 0 && (
                <div className="channel__play">
                  <button
                    className="channel__play--button channel__play--button-outline"
                    onClick={() => handlePlayClick()}
                  >
                    <PlayArrow />
                    {messages["app.home.song.play"]}
                  </button>
                  <button
                    className="channel__play--button channel__play--button-outline"
                    onClick={handleShuffleClick}
                  >
                    <Shuffle />
                    {messages["app.songs.play.shuffle"]}
                  </button>
                </div>
              )}
            </div>
          </div>
          <div className="container">
            <h2 className="container__title  container__title--left">
              {messages["app.songs.title"]}
            </h2>
            {favorites.length > 0 ? (
              <TableList items={favorites} onPlayClick={handlePlayClick} />
            ) : (
              <div className="no-results-found__message container">
                <p className="no-results-found__message--title">
                  {messages["app.data.not-found.title"]}
                </p>
              </div>
            )}
          </div>
        </div>
      )}
    </>
  );
};

export default injectIntl(Favorites);
