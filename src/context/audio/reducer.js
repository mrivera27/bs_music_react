import {
  SET_SONGS,
  SET_QUEUE,
  CURRENT_SONG,
  TOGGLE_RANDOM,
  TOGGLE_REPEAT,
  TOGGLE_PLAYING,
  SONG_END,
} from "../actions";

export default (state, action) => {
  switch (action.type) {
    case SET_SONGS:
      return {
        ...state,
        songs: action.payload,
      };
    case SET_QUEUE:
      return {
        ...state,
        queue: action.payload,
      };
    case CURRENT_SONG:
      return {
        ...state,
        current: action.payload,
        playing: true,
      };
    case TOGGLE_RANDOM:
      return {
        ...state,
        random: action.payload,
      };
    case TOGGLE_REPEAT:
      return {
        ...state,
        repeat: action.payload,
      };
    case TOGGLE_PLAYING:
      return {
        ...state,
        playing: action.payload,
      };
    case SONG_END:
      return {
        ...state,
        playing: action.payload,
      };
    default:
      return state;
  }
};
