import React, { useContext } from "react";
import { withRouter } from "react-router-dom";

import TopNav from "../containers/TopNav";
import Footer from "../containers/Footer";
import { audioContext } from "../context/contexts";

const AppLayout = ({ children }) => {
  const { songs } = useContext(audioContext);

  return (
    <>
      <TopNav />
      <main className="app-layout">
        <div className="container-fluid">{children}</div>
      </main>
      {songs.length > 0 && <Footer />}
    </>
  );
};
export default withRouter(AppLayout);
