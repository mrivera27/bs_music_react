import { SIGNIN_USER, SIGNOUT_USER, SIGNUP_USER } from "../actions";
import { setCurrentUser, setAuthToken } from "../../helpers/Utils";

export const signinUser = (user, authToken) => {
  setCurrentUser(user);
  setAuthToken(authToken);
  return { type: SIGNIN_USER, payload: user };
};

export const signupUser = (user, authToken) => {
  setCurrentUser(user);
  setAuthToken(authToken);
  return { type: SIGNUP_USER, payload: user };
};

export const signoutUser = () => {
  setCurrentUser();
  setAuthToken();
  return {
    type: SIGNOUT_USER,
  };
};
