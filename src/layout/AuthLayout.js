import React from "react";

const AuthLayout = ({ children }) => {
  return (
    <main className="auth-layout">
      <div className="container-fluid auth-layout__container">{children}</div>
    </main>
  );
};

export default AuthLayout;
