import React, { useContext, useState } from "react";
import { useHistory, Link } from "react-router-dom";
import { injectIntl, useIntl } from "react-intl";

import { authContext } from "../../context/contexts";
import Form from "../../components/Form";
import Logo from "../../components/Logo";
import Alert from "../../components/Alert";

const INIT_STATE = {
  email: "",
  password: "",
  displayName: "",
};

const Signup = () => {
  const { signupUser } = useContext(authContext);
  const [values, setValues] = useState(INIT_STATE);
  const history = useHistory();
  const { messages } = useIntl();
  const [alert, setAlert] = useState();

  const handleChange = (event) => {
    event.persist();
    setValues((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };

  const inputs = [
    {
      type: "text",
      placeholder: messages["user.signin.placeholder.name"],
      name: "displayName",
      value: values.displayName,
      onChange: handleChange,
    },
    {
      type: "text",
      placeholder: messages["user.signin.placeholder.email"],
      name: "email",
      value: values.email,
      onChange: handleChange,
    },
    {
      type: "password",
      placeholder: messages["user.signin.placeholder.password"],
      name: "password",
      value: values.password,
      onChange: handleChange,
    },
  ];

  const handleSubmit = async (event) => {
    event.preventDefault();
    const response = await signupUser(values);
    response.code ? setAlert(response.message) : history.go(0);
  };

  return (
    <>
      {alert && <Alert message={alert} setMessage={setAlert} />}
      <div className="user-layout__container--top">
        <Logo revert={true} />
      </div>
      <div className="user-layout__container--bottom container">
        <h1 className="signin__welcome--title">
          {messages["user.signup.submit.text"]}
        </h1>
        <p className="signin__welcome--text">
          {messages["user.signup.welcome.text"]}
        </p>
        <div className="signin__form">
          <Form
            inputs={inputs}
            buttonText={messages["user.signup.submit.text"]}
            onSubmit={handleSubmit}
          />
          <div className="signin__footer--redirect">
            <hr className="signin__footer--divider" />
            <p>{messages["user.signup.already.text"]}</p>
            <Link to="/auth/signin" className="user-layout__container--link">
              {messages["user.signin.submit.text"]}
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default injectIntl(Signup);
