import React, { useEffect } from "react";

const Alert = ({ message, setMessage }) => {
  useEffect(() => {
    setTimeout(function () {
      setMessage();
    }, 1500);
  }, [setMessage]);

  return (
    <div className="alert alert__container">
      <p className="alert__message">{message}</p>
    </div>
  );
};

export default Alert;
