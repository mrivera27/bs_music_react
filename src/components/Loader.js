import React, { Suspense } from "react";
import PropTypes from "prop-types";

const Loader = ({ children }) => {
  return <Suspense fallback={<div className="loading" />}>{children}</Suspense>;
};

Loader.propTypes = {
  children: PropTypes.object,
};

export default Loader;
