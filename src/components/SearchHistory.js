import React, { useContext, useState, useEffect } from "react";
import { injectIntl, useIntl } from "react-intl";

import Firebase from "../helpers/Firebase";
import { authContext } from "../context/contexts";

const SearchHistory = ({ show }) => {
  const { messages } = useIntl();
  const { auth } = useContext(authContext);
  const [history, setHistory] = useState();

  useEffect(() => {
    if (auth) {
      Firebase.getOrderedBy(
        `search-history/${auth}`,
        "timestamp",
        (snapshot) => {
          let data = [];
          snapshot.forEach((snap) => {
            data.push({ ...snap.val(), id: snap.key });
          });
          setHistory(data.reverse());
        }
      );
    }
  }, []);

  return (
    <ul className={show ? "d-flex" : "d-none"}>
      {history && history.map(({ id, query }) => <li key={id}>{query}</li>)}
    </ul>
  );
};

export default injectIntl(SearchHistory);
