// GENERAL
export const APP_ROOT = "/";
// AUTH
export const AUTH_COOKIE_KEY = "__tk_auth";
export const FIREBASE_CONFIG = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_FIREBASE_APP_ID,
  measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENT_ID,
};
export const USER_STORAGE_KEY = "__current_user";
// THEME
export const COLORS = [
  "blue",
  "green",
  "orange",
  "pink",
  "purple",
  "red",
  "teal",
  "yellow",
];
export const DEFAULT_COLOR = "light.green";
export const THEME_COLOR_STORAGE_KEY = "__theme_selected_color";
export const THEME_MODES = ["dark", "light"];
export const THEME_TYPES = ["icon", "color"];
// LOCALE
export const DEFAULT_LOCALE = "en";
export const LOCALE_OPTIONS = [
  { id: "en", name: "English" },
  { id: "es", name: "Español" },
];
export const LOCALE_STORAGE_KEY = "__locale_selected_language";
