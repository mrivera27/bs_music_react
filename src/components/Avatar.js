import React from "react";
import { Person } from "@material-ui/icons";

const Avatar = ({ size, className, onClick }) => {
  return (
    <div className={`avatar avatar--${size} ${className}`} onClick={onClick}>
      <Person className="avatar__icon" />
    </div>
  );
};

export default Avatar;
