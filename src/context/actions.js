/* SETTINGS */
export const CHANGE_LOCALE = "CHANGE_LOCALE";
export const CHANGE_THEME = "CHANGE_THEME";

/* AUTH */
export const SIGNIN_USER = "SIGNIN_USER";
export const SIGNUP_USER = "SIGNUP_USER";
export const SIGNOUT_USER = "SIGNOUT_USER";

/* AUDIO */
export const CURRENT_SONG = "CURRENT_SONG";
export const SET_SONGS = "SET_SONGS";
export const TOGGLE_RANDOM = "TOGGLE_RANDOM";
export const TOGGLE_REPEAT = "TOGGLE_OPTIONS";
export const TOGGLE_PLAYING = "TOGGLE_PLAYING";
export const SONG_END = "SONG_END";
export const SET_QUEUE = "SET_QUEUE";
