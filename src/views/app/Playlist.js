import React, { useEffect, useContext } from "react";
import { injectIntl, useIntl } from "react-intl";
import { useHistory } from "react-router-dom";

import { audioContext } from "../../context/contexts";
import TableList from "../../components/TableList";
import { APP_ROOT } from "../../constants/defaultValues";

const Playlist = () => {
  const { messages } = useIntl();
  const { songs } = useContext(audioContext);
  const history = useHistory();

  useEffect(() => {
    if (songs.length === 0) history.push(APP_ROOT);
  });

  return (
    <>
      {songs.length > 0 && (
        <div className="container">
          <h2 className="container__title">{messages["app.playlist.title"]}</h2>
          <TableList items={songs} playlist={true} />
        </div>
      )}
    </>
  );
};

export default injectIntl(Playlist);
