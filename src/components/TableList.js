import React, { useContext, useEffect, useState } from "react";
import {
  PlayArrow,
  FavoriteBorder,
  QueueMusic,
  MoreVert,
  Favorite,
} from "@material-ui/icons";
import { injectIntl, useIntl } from "react-intl";

import Dropdown from "./Dropdown";
import { authContext, audioContext } from "../context/contexts";
import Firebase from "../helpers/Firebase";
import Alert from "./Alert";

const TableList = ({ items, onPlayClick, playlist }) => {
  const {
    songs,
    setSongs,
    togglePlaying,
    currentSong,
    queue,
    setQueue,
  } = useContext(audioContext);
  const [favorites, setFavorites] = useState([]);
  const { auth } = useContext(authContext);
  const { messages } = useIntl();
  const [alert, setAlert] = useState();

  useEffect(() => {
    if (auth) {
      Firebase.getAll(`favorites/${auth}`, (snapshot) => {
        let data = [];
        snapshot.forEach((snap) => {
          data.push(snap.val().id);
        });
        setFavorites(data);
      });
    }
  }, [auth]);

  const handleFavClick = (item) => {
    if (!auth) return;
    const { favorite, ...data } = item;
    const index = favorites.indexOf(item.id);
    if (index > -1) {
      let auxList = [...favorites];
      auxList.splice(index, 1);
      setFavorites(auxList);
      Firebase.getByValue(`favorites/${auth}`, "id", item.id, (snapshot) => {
        snapshot.forEach(({ key }) => {
          Firebase.removeData(`favorites/${auth}/${key}`);
        });
      });
    } else {
      setFavorites([...favorites, item.id]);
      Firebase.setData("favorites", data, auth);
    }
  };

  const handleQueueClick = (item) => {
    const index = songs.findIndex(({ id }) => item.id === id);
    if (index > -1) {
      if (songs.length > 1 && index === currentSong) {
        setAlert(messages["app.songs.queue.remove.alert"]);
        return;
      }
      let auxList = [...songs];
      auxList.splice(index, 1);
      setQueue(auxList.map(({ id }) => id));
      setSongs(auxList);
    } else {
      setQueue([...queue, item.id]);
      setSongs([...songs, item]);
    }
    togglePlaying(true);
  };

  const moreOptions = (item) => {
    let opts = [];
    if (auth) {
      opts.push({
        text:
          favorites.indexOf(item.id) > -1
            ? messages["app.songs.favorites.remove"]
            : messages["app.songs.favorites.add"],
        onClick: () => handleFavClick(item),
        icon:
          favorites.indexOf(item.id) > -1 ? <Favorite /> : <FavoriteBorder />,
      });
    }
    if (!playlist) {
      opts.push({
        text:
          queue.indexOf(item.id) > -1
            ? messages["app.songs.queue.remove"]
            : messages["app.songs.queue.add"],
        onClick: () => handleQueueClick(item),
        icon: <QueueMusic />,
      });
    }
    return opts;
  };

  return (
    <div className="table-list">
      {alert && <Alert message={alert} setMessage={setAlert} />}
      {items &&
        items.map((item) => (
          <div
            key={item.id}
            className={`table-list__row ${
              playlist && songs[currentSong].id === item.id
                ? "table-list__row--selected"
                : ""
            }`}
          >
            {!playlist && (
              <div className="table-list__data data__play table-list__data--xs">
                <div
                  className="table-list__data--play"
                  onClick={() => onPlayClick(item.id)}
                >
                  <PlayArrow />
                </div>
              </div>
            )}
            <div className="data__content">
              <div className="table-list__data table-list__data--lg data__content--title">
                {item.name}
              </div>
              <div className="table-list__data data__content--display-xs">
                <span className="data__content-xs--subtitle">
                  {item.channel} • {item.duration}
                </span>
              </div>
              <div className="table-list__data data__content--display-sm table-list__data--xl">
                <div className="table-list__data--md">
                  <span>{item.channel}</span>
                </div>
                <div className="table-list__data--sm">
                  <span>{item.duration}</span>
                </div>
              </div>
            </div>
            <div className="data__options">
              {auth && (
                <div
                  className={`table-list__data table-list__data--xs data__options--favorites ${
                    favorites.indexOf(item.id) > -1
                      ? "data__options--active"
                      : ""
                  }`}
                  onClick={() => handleFavClick(item)}
                >
                  {favorites.indexOf(item.id) > -1 ? (
                    <Favorite />
                  ) : (
                    <FavoriteBorder />
                  )}
                </div>
              )}
              {!playlist && (
                <div
                  className={`table-list__data table-list__data--xs data__options--queue ${
                    queue.indexOf(item.id) > -1 ? "data__options--active" : ""
                  }`}
                  onClick={() => handleQueueClick(item)}
                >
                  <QueueMusic />
                </div>
              )}
              <div className="table-list__data table-list__data--xs data__options--more">
                <Dropdown options={moreOptions(item)}>
                  <MoreVert className="dropdown__button" />
                </Dropdown>
              </div>
            </div>
          </div>
        ))}
    </div>
  );
};

export default injectIntl(TableList);
