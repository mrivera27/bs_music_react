import { SIGNIN_USER, SIGNOUT_USER, SIGNUP_USER } from "../actions";
import { getCurrentUser, getAuthToken } from "../../helpers/Utils";

const INIT_STATE = {
  user: getCurrentUser(),
  auth: getAuthToken(),
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case SIGNIN_USER:
      return { ...state, user: action.payload.user, auth: action.payload.auth };
    case SIGNUP_USER:
      return { ...state, user: action.payload.user, auth: action.payload.auth };
    case SIGNOUT_USER:
      return { ...state, user: null, auth: null };
    default:
      return { ...state };
  }
};
