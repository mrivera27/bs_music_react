import React, { useContext } from "react";
import { IntlProvider } from "react-intl";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";

import { localeContext } from "./context/contexts";
import AppLocale from "./lang";
import Loader from "./components/Loader";

const ViewApp = React.lazy(() => import("./views/app"));
const ViewAuth = React.lazy(() => import("./views/auth"));
const ViewError = React.lazy(() => import("./views/Error"));

function App() {
  const { locale } = useContext(localeContext);
  const currentAppLocale = AppLocale[locale];

  return (
    <IntlProvider
      locale={currentAppLocale.locale}
      messages={currentAppLocale.messages}
    >
      <Loader>
        <Router>
          <Switch>
            <Route path="/error" exact>
              <ViewError />
            </Route>
            <Route path="/auth">
              <ViewAuth />
            </Route>
            <Route path="/">
              <ViewApp />
            </Route>
            <Redirect to="/error" />
          </Switch>
        </Router>
      </Loader>
    </IntlProvider>
  );
}

export default App;
