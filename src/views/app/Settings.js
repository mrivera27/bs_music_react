import React, { useContext } from "react";
import { WbSunny, NightsStay } from "@material-ui/icons";
import { injectIntl, useIntl } from "react-intl";
import { capitalize } from "@material-ui/core";

import { COLORS, THEME_MODES } from "../../constants/defaultValues";
import Theme from "../../components/Theme";
import { localeContext } from "../../context/contexts";
import { LOCALE_OPTIONS } from "../../constants/defaultValues";

const Settings = () => {
  const { messages } = useIntl();
  const { locale, changeLocale } = useContext(localeContext);
  const modeIcon = { dark: <NightsStay />, light: <WbSunny /> };

  return (
    <div className="container">
      <h2>{messages["app.settings.general.title"]}</h2>
      <div className="settings settings__container">
        <div className="locale">
          <p>{`${messages["app.settings.locale.title"]}: ${capitalize(
            messages[`app.settings.locale.${locale || "none"}`]
          )}`}</p>
          <ul>
            {LOCALE_OPTIONS &&
              LOCALE_OPTIONS.map(({ id }) => (
                <li
                  key={id}
                  onClick={() => changeLocale(id)}
                  className={`locale__text ${
                    id === locale ? "locale__text--selected" : ""
                  }`}
                >
                  <div className="locale__text--id text__circle text__circle--theme">
                    {id.toUpperCase()}
                  </div>
                  {capitalize(messages[`app.settings.locale.${id}`])}
                </li>
              ))}
          </ul>
        </div>
      </div>
      <h2>{messages["app.settings.theme.title"]}</h2>
      <div className="settings settings__container">
        <Theme
          text={messages["app.settings.theme.background"]}
          type={messages["app.settings.theme.icon-type"]}
          options={THEME_MODES}
          modeIcon={modeIcon}
        />
        <Theme
          text={messages["app.settings.theme.color"]}
          type={messages["app.settings.theme.color-type"]}
          options={COLORS}
        />
      </div>
    </div>
  );
};

export default injectIntl(Settings);
