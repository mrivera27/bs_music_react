import React, { useContext, useEffect, useState } from "react";
import { injectIntl, useIntl } from "react-intl";
import { useLocation } from "react-router-dom";
import { Settings, ExitToApp, MeetingRoom } from "@material-ui/icons";

import { authContext } from "../context/contexts";
import SearchBar from "../components/SearchBar";
import Logo from "../components/Logo";
import Dropdown from "../components/Dropdown";
import Avatar from "../components/Avatar";

const queryString = require("querystring");

const TopNav = () => {
  const { messages } = useIntl();
  const { user, signoutUser } = useContext(authContext);
  const location = useLocation();
  const [query, setQuery] = useState("");

  useEffect(() => {
    const searchQuery = queryString.parse(location.search.substr(1)).q;
    setQuery(searchQuery || "");
  }, [location.search]);

  const userOptions = [
    {
      text: messages["app.top-nav.settings"],
      to: "/settings",
      icon: <Settings />,
    },
    user
      ? {
          text: messages["app.top-nav.log-out"],
          to: "",
          icon: <ExitToApp />,
          onClick: signoutUser,
        }
      : {
          text: messages["app.top-nav.log-in"],
          to: "/auth/signin",
          icon: <MeetingRoom />,
        },
  ];

  return (
    <header className="top-nav">
      <div className="top-nav__item top-nav__item--left">
        <Logo />
      </div>
      <div className="top-nav__item top-nav__item--center">
        <SearchBar
          to={`/results?q=${query}`}
          initValue={query}
          onChange={({ target }) => setQuery(target.value)}
        />
      </div>
      <div className="top-nav__item top-nav__item--right">
        <div className="top-nav__user">
          <span className="top-nav__user--display-name">
            {(user && user.displayName) || messages["app.top-nav.logged.not"]}
          </span>
          <Dropdown options={userOptions}>
            <Avatar className="dropdown__button" />
          </Dropdown>
        </div>
      </div>
    </header>
  );
};

export default injectIntl(TopNav);
