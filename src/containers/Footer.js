import React from "react";
import { injectIntl } from "react-intl";

import AudioPlayer from "../components/AudioPlayer";

const Footer = () => {
  return (
    <footer className="footer">
      <AudioPlayer />
    </footer>
  );
};

export default injectIntl(Footer);
