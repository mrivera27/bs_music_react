import { CHANGE_THEME } from "../actions";
import { setCurrentColor } from "../../helpers/Utils";

export const changeTheme = (theme) => {
  setCurrentColor(theme);
  return {
    type: CHANGE_THEME,
    payload: theme,
  };
};
