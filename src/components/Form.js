import React from "react";

const Form = ({ inputs, buttonText, onSubmit }) => {
  return (
    <form onSubmit={onSubmit} className="form">
      {inputs &&
        inputs.map(({ type, placeholder, name, value, onChange }, index) => (
          <input
            key={`${placeholder}-input-${index}`}
            type={type}
            placeholder={placeholder}
            name={name}
            value={value}
            className="form__input"
            onChange={onChange}
          />
        ))}
      <input
        type="submit"
        value={buttonText}
        className="form__submit form__submit--block form__submit--outline"
      />
    </form>
  );
};

export default Form;
