import React, { useState, useEffect, useRef, useContext } from "react";
import { audioContext } from "../context/contexts";
import {
  SkipPrevious,
  PlayArrow,
  Pause,
  SkipNext,
  Repeat,
  Shuffle,
  VolumeUp,
  ExpandLess,
  ExpandMore,
} from "@material-ui/icons";
import { Link, useHistory, useLocation } from "react-router-dom";

const AudioPlayer = () => {
  const {
    currentSong,
    songs,
    nextSong,
    prevSong,
    repeat,
    random,
    playing,
    toggleRandom,
    toggleRepeat,
    togglePlaying,
    handleSongEnd,
  } = useContext(audioContext);
  const audio = useRef("audio_tag");
  const [volume, setVolume] = useState(0.3);
  const [duration, setDuration] = useState(0);
  const [currentTime, setCurrentTime] = useState(0);
  const [showPlaylist, setShowPlaylist] = useState(false);
  const location = useLocation();
  const history = useHistory();

  const fmtMSS = (s) => (s - (s %= 60)) / 60 + (10 <= s ? ":" : ":0") + ~~s;

  const toggleAudio = () =>
    audio.current.paused ? audio.current.play() : audio.current.pause();

  const handleVolume = (event) => {
    const vol = event.target.value / 100;
    setVolume(vol);
    audio.current.volume = vol;
  };

  const handleProgress = (event) => {
    const compute = (event.target.value * duration) / 100;
    setCurrentTime(compute);
    audio.current.currentTime = compute;
  };

  useEffect(() => {
    audio.current.volume = volume;
    if (playing) toggleAudio();
  }, [currentSong, playing, volume]);

  useEffect(() => {
    if (playing) audio.current.play();
  }, [songs, playing]);

  useEffect(() => {
    setShowPlaylist(location.pathname === "/playlist");
  }, [location, showPlaylist]);

  return (
    <div className="player-container">
      {songs && (
        <>
          <audio
            onTimeUpdate={({ target }) => setCurrentTime(target.currentTime)}
            onCanPlay={({ target }) => setDuration(target.duration)}
            onEnded={handleSongEnd}
            ref={audio}
            type="audio/mp3"
            preload="true"
            src={songs[currentSong]?.url}
          />
          <div className="player__progress">
            <input
              onChange={handleProgress}
              value={duration ? (currentTime * 100) / duration : 0}
              type="range"
              name="progres-bar"
              className="player__progress--bar"
            />
          </div>
          <div className="player">
            <div className="player--left">
              <div className="player__controls player-controls">
                <button
                  className="player-controls__button player-controls__button--prev"
                  onClick={prevSong}
                >
                  <SkipPrevious />
                </button>
                <button
                  className={`player-controls__button player-controls__button--${
                    playing ? "pause" : "play"
                  }`}
                  onClick={() => {
                    togglePlaying(!playing);
                    toggleAudio();
                  }}
                >
                  {playing ? <Pause /> : <PlayArrow />}
                </button>
                <button
                  className="player-controls__button player-controls__button--next"
                  onClick={nextSong}
                >
                  <SkipNext />
                </button>
              </div>
              <div className="player__time">
                <span className="player__time--current">
                  {fmtMSS(currentTime)}
                </span>
                <span className="player__time--divider">/</span>
                <span className="player__time--total">{fmtMSS(duration)}</span>
              </div>
            </div>
            <div className="player--center player__song">
              <Link
                to={showPlaylist ? "#" : "/playlist"}
                onClick={() => {
                  setShowPlaylist(!showPlaylist);
                  if (location.pathname === "/playlist") history.goBack();
                }}
              >
                {showPlaylist ? <ExpandMore /> : <ExpandLess />}
                <p className="player__song--name">{songs[currentSong]?.name}</p>
                <span className="player__song--other">
                  {songs[currentSong]?.channel}
                </span>
              </Link>
            </div>
            <div className="player--right">
              <div className="player__volume">
                <VolumeUp className="player__volume--icon" />
                <input
                  value={Math.round(volume * 100)}
                  type="range"
                  name="vol-bar"
                  className="player__volume--bar"
                  onChange={handleVolume}
                />
              </div>
              <div className="player__options player-options">
                <button
                  className={`player-options__button player-options__button--random ${
                    random ? "player-options__button--active" : ""
                  }`}
                  onClick={toggleRandom}
                >
                  <Shuffle />
                </button>
                <button
                  className={`player-options__button player-options__button--repeat ${
                    repeat ? "player-options__button--active" : ""
                  }`}
                  onClick={toggleRepeat}
                >
                  <Repeat />
                </button>
              </div>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default AudioPlayer;
