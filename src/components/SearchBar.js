import React, { useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import { Search } from "@material-ui/icons";
import { injectIntl, useIntl } from "react-intl";

import { authContext } from "../context/contexts";
import Firebase from "../helpers/Firebase";

const SearchBar = ({ to, onChange, initValue }) => {
  const history = useHistory();
  const { messages } = useIntl();
  const { auth } = useContext(authContext);

  const handleKeyUp = (event) => {
    if (event.key === "Enter") {
      saveSearch();
      history.push(to);
    }
  };

  const saveSearch = () => {
    if (!auth) return;
    Firebase.setData(
      "search-history",
      {
        query: initValue,
        timestamp: Date.now(),
      },
      auth
    );
  };

  return (
    <div className="search-bar ">
      <input
        className="search-bar__input"
        type="text"
        placeholder={messages["app.top-nav.placeholder.search"]}
        onChange={onChange}
        onKeyUp={handleKeyUp}
        value={initValue}
      />
      <Link
        to={to}
        onClick={saveSearch}
        className="search-bar__link search-bar__link--append"
      >
        <Search />
      </Link>
    </div>
  );
};

export default injectIntl(SearchBar);
