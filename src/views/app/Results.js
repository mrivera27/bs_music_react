import React, { useEffect, useState, useContext } from "react";
import { injectIntl, useIntl } from "react-intl";
import { useLocation, useHistory } from "react-router-dom";

import { APP_ROOT } from "../../constants/defaultValues";
import Firebase from "../../helpers/Firebase";
import CardList from "../../components/CardList";
import TableList from "../../components/TableList";
import { audioContext } from "../../context/contexts";

const queryString = require("querystring");

const Results = () => {
  const { messages } = useIntl();
  const location = useLocation();
  const history = useHistory();
  const [channels, setChannels] = useState([]);
  const [resultSongs, setResultSongs] = useState([]);
  const { setSongs, setCurrent, togglePlaying } = useContext(audioContext);

  useEffect(() => {
    const searchQuery = queryString.parse(location.search.substr(1)).q;
    if (searchQuery) {
      getData(
        "channels",
        ({ name }) => name.toLowerCase().includes(searchQuery),
        (data) => setChannels(data)
      );
      getData(
        "songs",
        ({ name, channel, album }) =>
          `${name}${channel}${album}`.toLowerCase().includes(searchQuery),
        (data) => setResultSongs(data)
      );
    } else {
      history.push(APP_ROOT);
    }
  }, [location.search, history]);

  const getData = (ref, checkValue, setData) => {
    Firebase.getOrderedBy(ref, "name", (snapshot) => {
      let data = [];
      snapshot.forEach((snap) => {
        const values = snap.val();
        if (!checkValue(values)) return;
        data.push({ ...values, id: snap.key });
      });
      setData(data);
    });
  };

  const handlePlayClick = (songId) => {
    const song = resultSongs.findIndex(({ id }) => id === songId);
    setCurrent(song);
    togglePlaying(true);
    setSongs(resultSongs);
  };

  return (
    <div className="container">
      {channels.length > 0 && (
        <>
          <h2 className="container__title  container__title--left">
            {messages["app.channels.title"]}
          </h2>
          <CardList items={channels} wrapStyle="row--inline" />
        </>
      )}
      {resultSongs.length > 0 && (
        <>
          <h2 className="container__title  container__title--left">
            {messages["app.songs.title"]}
          </h2>
          <TableList items={resultSongs} onPlayClick={handlePlayClick} />
        </>
      )}
      {channels.length === 0 && resultSongs.length === 0 && (
        <div className="no-results-found__message">
          <h3 className="no-results-found__message--title">
            {messages["app.results.not-found.title"]}
          </h3>
          <p className="no-results-found__message--text">
            {messages["app.results.not-found.text"]}
          </p>
        </div>
      )}
    </div>
  );
};

export default injectIntl(Results);
