import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";

import { FIREBASE_CONFIG } from "../constants/defaultValues";

class Firebase {
  constructor() {
    firebase.initializeApp(FIREBASE_CONFIG);
    this.auth = firebase.auth();
    this.db = firebase.database();
  }

  async signin(email, password) {
    const user = await this.auth
      .signInWithEmailAndPassword(email, password)
      .catch((error) => error);
    return user;
  }

  async signup(email, password, displayName) {
    let user = await this.auth
      .createUserWithEmailAndPassword(email, password)
      .catch((error) => error);
    if (user) await user.user.updateProfile({ displayName: displayName });
    return user;
  }

  async signout() {
    return await this.auth.signOut().catch((error) => error);
  }

  getRealtime(ref, onDataCallback) {
    return this.db.ref(ref).on("value", onDataCallback);
  }

  getAll(ref, onDataCallback) {
    return this.db.ref(ref).once("value", onDataCallback);
  }

  getOrderedBy(ref, path, onDataCallback) {
    return this.db.ref(ref).orderByChild(path).once("value", onDataCallback);
  }

  getByValue(ref, path, value, onDataCallback) {
    return this.db
      .ref(ref)
      .orderByChild(path)
      .equalTo(value)
      .once("value", onDataCallback);
  }

  setData(ref, data, id = null) {
    const dbRef = this.db.ref(id ? `${ref}/${id}` : ref);
    return dbRef.push(data).key;
  }

  removeData(ref) {
    return this.db.ref(ref).remove();
  }
}

export default new Firebase();
