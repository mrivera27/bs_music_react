import React, { useEffect, useState, useContext } from "react";
import { injectIntl, useIntl } from "react-intl";
import { useRouteMatch } from "react-router-dom/cjs/react-router-dom.min";

import TableList from "../../components/TableList";
import Firebase from "../../helpers/Firebase";
import { PlayArrow, Shuffle } from "@material-ui/icons";
import { audioContext } from "../../context/contexts";
import { getRandomInt } from "../../helpers/Utils";

const Channel = () => {
  const { messages } = useIntl();
  const { params } = useRouteMatch();
  const [channel, setChannel] = useState();
  const [channelSongs, setChannelSongs] = useState([]);
  const { setSongs, setCurrent, togglePlaying } = useContext(audioContext);

  useEffect(() => {
    Firebase.getByValue("songs", "channel-id", params.channelId, (snapshot) => {
      let data = [];
      snapshot.forEach((snap) => {
        data.push({ ...snap.val(), id: snap.key });
      });
      setChannelSongs(data);
    });
  }, [params.channelId]);

  useEffect(() => {
    Firebase.getAll(`channels/${params.channelId}`, (snapshot) => {
      setChannel({ ...snapshot.val(), id: snapshot.key });
    });
  }, [params.channelId]);

  const handleShuffleClick = () => {
    const auxSongs = [...channelSongs];
    let shuffleList = [];
    while (shuffleList.length < channelSongs.length) {
      const index = getRandomInt(auxSongs.length);
      shuffleList.push(auxSongs.splice(index, 1)[0]);
    }
    setCurrent(0);
    togglePlaying(true);
    setSongs(shuffleList);
  };

  const handlePlayClick = (songId) => {
    const song = songId ? channelSongs.findIndex(({ id }) => id === songId) : 0;
    setCurrent(song);
    togglePlaying(true);
    setSongs(channelSongs);
  };

  return (
    <>
      {channel && channelSongs && (
        <div className="channel">
          <div className="container-fluid__overlay">
            <img
              src={channel?.poster?.image}
              alt={"" + channel?.name}
              width="100%"
              height="10%"
              className="channel__image"
            />
            <div className="container">
              <h2 className="container__title channel__title">
                {channel.name}
              </h2>
              <p className="channel__description">{channel.description}</p>
              <div className="channel__play">
                <button
                  className="channel__play--button channel__play--button-outline"
                  onClick={() => handlePlayClick()}
                >
                  <PlayArrow />
                  {messages["app.home.song.play"]}
                </button>
                <button
                  className="channel__play--button channel__play--button-outline"
                  onClick={handleShuffleClick}
                >
                  <Shuffle />
                  {messages["app.songs.play.shuffle"]}
                </button>
              </div>
            </div>
          </div>
          {channelSongs.length > 0 && (
            <div className="container">
              <h2 className="container__title  container__title--left">
                {messages["app.songs.title"]}
              </h2>
              <TableList items={channelSongs} onPlayClick={handlePlayClick} />
            </div>
          )}
        </div>
      )}
    </>
  );
};

export default injectIntl(Channel);
