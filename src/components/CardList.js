import React from "react";

import CardLink from "./CardLink";

const CardList = ({ items, children, wrapStyle }) => {
  return (
    <div className={`row ${wrapStyle}`}>
      {children}
      {items.map(({ id, poster, name }) => (
        <CardLink
          key={id}
          to={`/channel/${id}`}
          image={poster.thumb}
          title={name}
          className="row__item"
        />
      ))}
    </div>
  );
};

export default CardList;
