import React, { useContext } from "react";
import { Route, Switch, Redirect, useRouteMatch } from "react-router-dom";

import AuthLayout from "../../layout/AuthLayout";
import Loader from "../../components/Loader";
import { authContext } from "../../context/contexts";

const Signin = React.lazy(() => import("./Signin"));
const Signup = React.lazy(() => import("./Signup"));

const Auth = () => {
  const { auth } = useContext(authContext);
  const match = useRouteMatch();

  return (
    <AuthLayout>
      <Loader>
        <Switch>
          {auth && <Redirect to="/" />}
          <Redirect exact from={`${match.path}/`} to={`${match.path}/signin`} />
          <Route exact path={`${match.path}/signin`}>
            <Signin />
          </Route>
          <Route exact path={`${match.path}/signup`}>
            <Signup />
          </Route>
          <Redirect to="/error" />
        </Switch>
      </Loader>
    </AuthLayout>
  );
};

export default Auth;
