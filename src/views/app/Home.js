import React, { useContext, useEffect, useState } from "react";
import { injectIntl, useIntl } from "react-intl";
import { LibraryMusic } from "@material-ui/icons";

import CardList from "../../components/CardList";
import CardLink from "../../components/CardLink";
import Firebase from "../../helpers/Firebase";
import { authContext } from "../../context/contexts";

const Home = () => {
  const { messages } = useIntl();
  const [channels, setChannels] = useState([]);
  const { auth } = useContext(authContext);

  useEffect(() => {
    Firebase.getAll("channels", (snapshot) => {
      let data = [];
      snapshot.forEach((snap) => {
        data.push({ ...snap.val(), id: snap.key });
      });
      setChannels(data);
    });
  }, []);

  return (
    <div className="container">
      <h2 className="container__title">{messages["app.home.title"]}</h2>
      <CardList items={channels}>
        {auth && (
          <CardLink
            to={`/favorites?list=${auth}`}
            title={messages["app.home.favorites.title"]}
            className="row__item"
          >
            <div className="card-link__image card-link__image--favorites ">
              <LibraryMusic className="favorites__icon" />
            </div>
          </CardLink>
        )}
      </CardList>
    </div>
  );
};

export default injectIntl(Home);
