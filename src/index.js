import { DEFAULT_COLOR } from "./constants/defaultValues";
import { getCurrentColor, setCurrentColor } from "./helpers/Utils";

const color = getCurrentColor() || DEFAULT_COLOR;
setCurrentColor(color);

const render = () => {
  import(`./assets/css/sass/themes/bs.${color}.scss`).then(() => {
    require("./AppRenderer");
  });
};
render();
