import React, { useReducer } from "react";

import Context from "./context";
import reducer from "./reducer";
import { changeLocale } from "./actions";
import { getCurrentLanguage } from "../../helpers/Utils";

const INIT_STATE = {
  locale: getCurrentLanguage(),
};

const LocaleState = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, INIT_STATE);

  return (
    <Context.Provider
      value={{
        locale: state.locale,
        changeLocale: (locale) => dispatch(changeLocale(locale)),
      }}
    >
      {children}
    </Context.Provider>
  );
};

export default LocaleState;
