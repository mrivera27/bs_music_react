import React, { useReducer } from "react";

import Context from "./context";
import reducer from "./reducer";
import { changeTheme } from "./actions";
import { getCurrentColor } from "../../helpers/Utils";

const INIT_STATE = {
  theme: getCurrentColor(),
};

const ThemeState = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, INIT_STATE);

  return (
    <Context.Provider
      value={{
        theme: state.theme,
        changeTheme: (theme) => dispatch(changeTheme(theme)),
      }}
    >
      {children}
    </Context.Provider>
  );
};

export default ThemeState;
