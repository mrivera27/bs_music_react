import { capitalize } from "@material-ui/core";
import React, { useContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { injectIntl, useIntl } from "react-intl";

import { THEME_TYPES } from "../constants/defaultValues";
import { themeContext } from "../context/contexts";

const Theme = ({ text, type, options, modeIcon }) => {
  const { messages } = useIntl();
  const { theme, changeTheme } = useContext(themeContext);
  const history = useHistory();
  const [selected, setSelected] = useState("");

  useEffect(() => {
    setSelected(theme.split(".")[THEME_TYPES.indexOf(type)]);
  }, [type, theme]);

  const selectedClass = (option) =>
    selected === option ? `theme__${type}--selected` : "";

  const switchTheme = (value) => {
    let auxTheme = theme.split(".");
    auxTheme[THEME_TYPES.indexOf(type)] = value;
    setSelected(auxTheme[THEME_TYPES.indexOf(type)]);
    changeTheme(auxTheme.join("."));
    history.go(0);
  };

  return (
    <div className="theme">
      <p>{`${text}: ${capitalize(
        messages[`app.settings.theme.${selected || "none"}`]
      )}`}</p>
      <ul>
        {options &&
          options.map((option, index) => (
            <li
              key={`${type}-${index}`}
              onClick={() => switchTheme(option)}
              className={`theme__${type} ${selectedClass(option)}`}
            >
              <div className={`${type}__circle ${type}__circle--${option}`}>
                {type === "icon" && modeIcon[option]}
              </div>
              {capitalize(messages[`app.settings.theme.${option}`])}
            </li>
          ))}
      </ul>
    </div>
  );
};

export default injectIntl(Theme);
