import React, { useReducer } from "react";

import Context from "./context";
import reducer from "./reducer";
import { signinUser, signoutUser } from "./actions";
import { getCurrentUser, getAuthToken } from "../../helpers/Utils";
import Firebase from "../../helpers/Firebase";

const INIT_STATE = {
  user: getCurrentUser(),
  auth: getAuthToken(),
};

const AuthState = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, INIT_STATE);

  const signin = async ({ email, password }) => {
    const response = await Firebase.signin(email, password);
    if (response.user) {
      const { user } = response;
      var {
        phoneNumber,
        photoURL,
        providerId,
        uid,
        ...data
      } = user.providerData[0];
      dispatch(signinUser(data, user.uid));
    }
    return data || response;
  };

  const signup = async ({ email, password, displayName }) => {
    const response = await Firebase.signup(email, password, displayName);
    if (response.user) {
      const { user } = response;
      var {
        phoneNumber,
        photoURL,
        providerId,
        uid,
        ...data
      } = user.providerData[0];
      dispatch(signinUser(data, user.uid));
    }
    return data || response;
  };

  const signout = async () => {
    dispatch(signoutUser());
    Firebase.signout();
  };

  return (
    <Context.Provider
      value={{
        user: state.user,
        auth: state.auth,
        signinUser: signin,
        signupUser: signup,
        signoutUser: signout,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export default AuthState;
