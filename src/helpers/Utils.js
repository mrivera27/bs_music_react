import {
  AUTH_COOKIE_KEY,
  DEFAULT_COLOR,
  DEFAULT_LOCALE,
  LOCALE_OPTIONS,
  LOCALE_STORAGE_KEY,
  THEME_COLOR_STORAGE_KEY,
  USER_STORAGE_KEY,
} from "../constants/defaultValues.js";
import Cookies from "js-cookie";

export const getAuthToken = () => {
  try {
    return Cookies.get(AUTH_COOKIE_KEY);
  } catch (error) {
    console.log(">>> src/helpers/Utils.js: getAuthToken -> error", error);
    return "";
  }
};

export const getCurrentColor = () => {
  let currentColor = DEFAULT_COLOR;
  try {
    if (localStorage.getItem(THEME_COLOR_STORAGE_KEY)) {
      currentColor = localStorage.getItem(THEME_COLOR_STORAGE_KEY);
    }
  } catch (error) {
    console.log(">>>>: src/helpers/Utils.js : getCurrentColor -> error", error);
    currentColor = DEFAULT_COLOR;
  }
  return currentColor;
};

export const getCurrentLanguage = () => {
  let language = DEFAULT_LOCALE;
  try {
    language =
      localStorage.getItem(LOCALE_STORAGE_KEY) &&
      LOCALE_OPTIONS.find(
        (x) => x.id === localStorage.getItem(LOCALE_STORAGE_KEY)
      )
        ? localStorage.getItem(LOCALE_STORAGE_KEY)
        : DEFAULT_LOCALE;
  } catch (error) {
    console.log(
      ">>>>: src/helpers/Utils.js : getCurrentLanguage -> error",
      error
    );
    language = DEFAULT_LOCALE;
  }
  return language;
};

export const getCurrentUser = () => {
  try {
    return JSON.parse(localStorage.getItem(USER_STORAGE_KEY));
  } catch (error) {
    console.log(">>> src/helpers/Utils.js: getCurrentUser -> error", error);
    return "";
  }
};

export const setAuthToken = (token) => {
  try {
    token
      ? Cookies.set(AUTH_COOKIE_KEY, token)
      : Cookies.remove(AUTH_COOKIE_KEY);
  } catch (error) {
    console.log(">>> src/helpers/Utils.js: setAuthToken -> error", error);
  }
};

export const setCurrentColor = (color) => {
  try {
    color
      ? localStorage.setItem(THEME_COLOR_STORAGE_KEY, color)
      : localStorage.removeItem(THEME_COLOR_STORAGE_KEY);
  } catch (error) {
    console.log(">>>>: src/helpers/Utils.js : setCurrentColor -> error", error);
  }
};

export const setCurrentLanguage = (locale) => {
  try {
    locale
      ? localStorage.setItem(LOCALE_STORAGE_KEY, locale)
      : localStorage.removeItem(LOCALE_STORAGE_KEY);
  } catch (error) {
    console.log(
      ">>>>: src/helpers/Utils.js : setCurrentLanguage -> error",
      error
    );
  }
};

export const setCurrentUser = (user) => {
  try {
    user
      ? localStorage.setItem(USER_STORAGE_KEY, JSON.stringify(user))
      : localStorage.removeItem(USER_STORAGE_KEY);
  } catch (error) {
    console.log(">>> src/helpers/Utils.js: setCurrentUser -> error", error);
  }
};

export const getRandomInt = (max) =>
  Math.floor(Math.random() * Math.floor(max));
